# **Poly-M.E.R.S**
*Poly-Platform MQTT Events Response System*

## **Opis:**

-   Aplikacja mająca na celu ułatwienie komunikacji między poszczególnymi organami obsługi wydarzeń (głównie muzycznych oraz kulturalnych) bez konieczności używania komunikatów głosowych, które mogą zakłócać odbiór ww. wydarzenia.
-   Aplikacja napisana będzie głównie z myślą o urządzeniach z większymi wyświetlaczami lub urządzeniach dedykowanych.
-   Poprzez przyciski, małe pola tekstowe oraz grafiki imitujące diody kontrolne użytkownicy będą mogli przekazywać sobie informacje dotyczące zmian w programie, awarii sprzętu, jakości aspektów technicznych itd.
-   Dzięki użyciu protokołu MQTT aplikacja będzie mogłą być używana zarówno z urządzeniami mobilnymi, komputerami oraz prostymi mikrokontrolerami (Arduino, Rpi).

## **Funkcjonalności:**

-   Przesyłanie/odbieranie informacji przez protokół MQTT (publish/subscribe).
-   Zapis danych w bazie danych.
-   Zsynchronizowany timer/zegar.
-   Możliwość utworzenia i modyfikacji playlisty(API?).